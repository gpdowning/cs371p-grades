// ----------
// Grades.hpp
// ----------

#ifndef Grades_hpp
#define Grades_hpp

// --------
// includes
// --------

#include <algorithm> // count, min_element, transform
#include <cassert>   // assert
#include <map>       // map
#include <string>    // string
#include <vector>    // vector

using namespace std;

// -----------
// grades_eval
// -----------

string grades_eval (const vector<vector<int>>& v_v_scores) {
    assert(&v_v_scores);
    return "B-";}

#endif // Grades_hpp
