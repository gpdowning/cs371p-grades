// --------------
// run_Grades.cpp
// --------------

// --------
// includes
// --------

#include <iostream> // cin, cout, endl
#include <iterator> // back_inserter, istream_iterator
#include <sstream>  // istringstream
#include <string>   // string
#include <vector>   // vector

#include "Grades.hpp"

// -----------
// grades_read
// -----------

vector<vector<int>> grades_read () {
    // read newline
    string s;
    getline(cin, s);

    // v_scores
    vector<vector<int>> v_v_scores(5);

    // read scores
    for (int i = 0; i != 5; ++i) {
        getline(cin, s);
        istringstream iss(s);
        copy(istream_iterator<int>(iss), istream_iterator<int>(), back_inserter(v_v_scores[i]));}

    return v_v_scores;}

// ------------
// grades_print
// ------------

void grades_print (const string& letter) {
    cout << letter << endl;}

// ----
// main
// ----

int main () {
    // read number of test cases
    int n;
    cin >> n;

    // read newline
    string s;
    getline(cin, s);

    // read, eval, print (REPL)
    while (true) {
        const vector<vector<int>> v_v_scores = grades_read();
        if (!cin)
            break;
        const string letter = grades_eval(v_v_scores);
        grades_print(letter);}
    return 0;}

/*
sample input

2

2 2 2 2 0
2 1 1 2 2 2 2 2 2 3 3 0
1 2 2 2 2 2 2 2 2 2 2 2 0 3
2 2 2 1 2 2 2 2 2 3 2 3 0 0
2 2 2 2 1 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 3 2 2 2 3 2 2 3 2 3 2 2 2 0 0 0 0 0 0 0 0

3 3 3 3 2
1 3 2 1 2 3 2 3 3 0 3 0
3 2 2 3 3 2 3 3 3 2 2 3 2 3
2 3 3 2 3 3 2 2 2 2 3 3 2 2
2 3 1 2 2 2 3 1 1 3 1 3 1 1 3 1 2 1 2 3 3 3 1 2 3 1 1 2 3 3 1 2 1 3 1 3 3 2 3 2 2 1
*/

/*
sample output

B-
B-
*/
